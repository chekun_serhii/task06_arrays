package com.chekushka.comparator;

import java.util.Comparator;

public class Note {
    private String noteName;
    private int noteID;

    public Note(String noteName, int noteID) {
        this.noteName = noteName;
        this.noteID = noteID;

    }
    public String getNoteName() {
        return noteName;
    }

    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    public int getNoteID() {
        return noteID;
    }

    public void setNoteID(int noteID) {
        this.noteID = noteID;
    }

    @Override
    public String toString() {
        return "Note{" +
                "noteName='" + noteName + '\'' +
                ", noteID='" + noteID + '\'' +
                '}';
    }
    public static final Comparator<Note> SORT_BY_ID = new Comparator<Note>() {
        @Override
        public int compare(Note lhs, Note rhs) {
            return lhs.getNoteID() - rhs.getNoteID();
        }
    };
    public static final Comparator<Note> SORT_BY_NAME = new Comparator<Note>() {
        @Override
        public int compare(Note obj1, Note obj2) {
            return obj1.getNoteName().compareTo(obj2.getNoteName());
        }
    };
}
