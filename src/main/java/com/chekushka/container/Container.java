package com.chekushka.container;

import java.util.Arrays;

public class Container<T extends String> {
    private String[] array = new String[1];

    public Container() {
    }

    @SafeVarargs
    public Container(T... index) {
        addAll(index);
    }

    public String get(int index) {
        return this.array[index];
    }

    public String[] getAll() {
        return this.array;
    }

    public void add(int position, T element) {
        if (position > 0) {
            this.array = Arrays.copyOf(this.array, position + 1);
        }
        this.array[position] = element;
    }

    @SafeVarargs
    public final void addAll(T... index) {
        this.array = new String[index.length];
        System.arraycopy(index, 0, this.array, 0, index.length);
    }
}
