package com.chekushka.view;

import com.chekushka.comparator.Note;
import com.chekushka.container.Container;
import com.chekushka.controller.ArrayTaskControllerImpl;
import com.chekushka.controller.GameImpl;
import com.chekushka.controller.interfaces.ArrayTaskController;
import com.chekushka.model.GameDoor;
import com.chekushka.view.entries.MenuEntry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class AppView {
    private static Logger logger1 = LogManager.getLogger(AppView.class);
    private static ArrayTaskController ArrayControl = new ArrayTaskControllerImpl();
    private static GameImpl GameControl = new GameImpl();
    private static Scanner input = new Scanner(System.in);
    private static int[] firstArray = {1, 2, 2, 4, 6, 8, 3, 2, 4};
    private static int[] secondArray = {4, 3, 7, 5, 3, 8, 9};
    private static Container<String> newContainer = new Container<>();
    private static List<String> newList = new ArrayList<>();
    private static List<Note> list = new ArrayList<>();

    public final void show() {
        Menu menu = new Menu();
        menu.addEntry(new MenuEntry("Array logic tasks:") {
            @Override
            public void run() {
                logger1.info("A.a:"
                        + Arrays.toString(ArrayControl.similarElementsAll(firstArray, secondArray))
                        + "\n");
//               Logger1.info("A.b:"
//                        +Arrays.toString(ArrayControl.similarElementsNone(firstArray, secondArray))
//                        +"\n");
                logger1.info("B:"
                        + Arrays.toString(ArrayControl.deleteRepeatElements(firstArray))
                        + "\n");
                logger1.info("C:"
                        + Arrays.toString(ArrayControl.deleteElementSeries(firstArray))
                        + "\n");
            }
        });
        menu.addEntry(new MenuEntry("Array game") {
            @Override
            public void run() {
                GameDoor[] game;
                game = GameControl.setHeroWay();
                int counter = 0;
                boolean exit = false;
                boolean heroAlive;
                int chooseDoor;
                while (!exit) {
                    logger1.info("Choose the door number (from 1 to 10):");
                    chooseDoor = input.nextInt();
                    if (game[chooseDoor].isArtifact()) {
                        logger1.info("Oh! You`ve found an artifact! Good luck!");
                    } else {
                        logger1.info("Damn! A monster appeared!");
                    }
                    if (game[chooseDoor] != null) {
                        heroAlive = GameControl.gameStep(chooseDoor, game);
                        if (!heroAlive) {
                            logger1.info("Game over. You are dead!");
                            exit = true;
                        }
                    } else {
                        logger1.info("The door has already opened! Choose other option.");
                    }
                    counter = counter + 1;
                    if (counter == 10) {
                        logger1.info("You are won! Congratulations!!!");
                        exit = true;
                    }
                }
            }
        });
        menu.addEntry(new MenuEntry("Custom container test") {
            @Override
            public void run() {
                logger1.info("Started filling and printing "
                        + "custom container...");
                for (int i = 0; i < 99; i++) {
                    newContainer.add(i, "oh, hi MArk");
                }
                for (int i = 0; i < 99; i++) {
                    newContainer.get(i);
                }
                logger1.info("Finished Process");
                logger1.info("Started filling and printing "
                        + "Arraylist...");
                for (int i = 0; i < 99; i++) {
                    newList.add(i, "oh, hi MArk");
                }
                for (int i = 0; i < 99; i++) {
                    newList.get(i);
                }
                logger1.info("Finished Process");
            }
        });
        menu.addEntry(new MenuEntry("Test comparator") {
            @Override
            public void run() {
                Note obj1 = new Note("gdrg", 3);
                list.add(0, obj1);
                Note obj2 = new Note("bdhht", 1);
                list.add(1, obj2);
                Note obj3 = new Note("wagh", 4);
                list.add(2, obj3);
                Note obj4 = new Note("ltryk", 2);
                list.add(3, obj4);
                list.sort(Note.SORT_BY_NAME);
                logger1.info(list);
            }
        });
        menu.run();
    }
}
