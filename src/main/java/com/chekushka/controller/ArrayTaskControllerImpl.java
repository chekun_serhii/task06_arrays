package com.chekushka.controller;

import com.chekushka.controller.interfaces.ArrayTaskController;

public class ArrayTaskControllerImpl implements ArrayTaskController {


    @Override
    public int[] similarElementsAll(int[] firstArray, int[] secondArray) {
        int indexCount = 0;

        for (int value : firstArray) {
            for (int i : secondArray) {
                if (value == i) {
                    indexCount = indexCount + 1;
                }
            }
        }

        int[] finalArray = new int[indexCount];
        indexCount = 0;

        for (int item : firstArray) {
            for (int value : secondArray) {
                if (item == value) {
                    finalArray[indexCount] = value;
                    indexCount = indexCount + 1;
                }
            }
        }
        return finalArray;
    }

    @Override
    public int[] similarElementsNone(int[] firstArray, int[] secondArray) {
        int indexCount = 0;
        int[] finalArray = new int[firstArray.length];
        return finalArray;
    }

    @Override
    public int[] deleteRepeatElements(int[] arr) {
        int finLength = arr.length;

        for (int i = 0; i < finLength; i++) {
            for (int j = i + 1; j < finLength; j++) {
                if (arr[i] == arr[j]) {
                    int temp = j;
                    for (int k = j+1; k < finLength; k++, temp++) {
                        arr[temp] = arr[k];
                    }
                    finLength--;
                    j--;
                }
            }
        }

        int[] finalArr = new int[finLength];
        System.arraycopy(arr, 0, finalArr, 0, finLength);
        return finalArr;
    }

    @Override
    public int[] deleteElementSeries(int[] arr) {
        int[] temp = new int[arr.length];
        int finLength = arr.length;
        int j = 0;

        for(int i = 0; i < arr.length-1; i++){
            if(arr[i] != arr[i+1]){
                temp[j] = arr[i];
                j++;
            }
            else {
                finLength--;
            }
        }

        int[] finalArr = new int[finLength-1];
        System.arraycopy(temp, 0, finalArr, 0, finLength-1);
        return finalArr;
    }
}
