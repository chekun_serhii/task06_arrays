package com.chekushka.controller;

import com.chekushka.model.GameDoor;

import java.util.Random;

public class GameImpl {
    private Random random = new Random();
    private int heroPower = 25;
    private GameDoor[] heroWay = new GameDoor[10];
    private boolean isAlive = true;

    public boolean getIsAlive(){
        return isAlive;
    }

    public GameDoor[] setHeroWay(){
        int DOOR_NUM = 10;
        int doorNumber;
        boolean isArtifact;
        int power;
        for(int i = 0; i < DOOR_NUM; i++){
            doorNumber = i + 1;
            isArtifact = random.nextBoolean();
            if(isArtifact)
                power = random.nextInt((80 - 10)+1);
            else {
                power = random.nextInt((100 - 5)+1);
            }
            GameDoor newDoor = new GameDoor(doorNumber,isArtifact,power);
            heroWay[i] = newDoor;
        }
        return heroWay;
    }

    public boolean gameStep(int choice, GameDoor[] heroWay) {
        if(heroWay[choice].isArtifact()){
            heroPower = heroPower + heroWay[choice].getPower();
        }
        else {
            if(heroPower < heroWay[choice].getPower()) {
                isAlive = false;
            }
        }
        return isAlive;
    }
}
