package com.chekushka.controller.interfaces;

public interface ArrayTaskController {
    public int[] similarElementsAll(int[] firstArray, int[] secondArray);
    public int[] similarElementsNone(int[] firstArray, int[] secondArray);
    public int[] deleteRepeatElements(int[] firstArray);
    public int[] deleteElementSeries(int[] firstArray);

}
