package com.chekushka.model;

public class GameDoor {
    private int doorNumber;
    private boolean isArtifact;
    private int power;

    public GameDoor(int doorNumber, boolean isArtifact, int power) {
        this.doorNumber = doorNumber;
        this.isArtifact = isArtifact;
        this.power = power;
    }

    public int getDoorNumber() {
        return doorNumber;
    }

    public void setDoorNumber(int doorNumber) {
        this.doorNumber = doorNumber;
    }

    public boolean isArtifact() {
        return isArtifact;
    }

    public void setArtifact(boolean artifact) {
        isArtifact = artifact;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
