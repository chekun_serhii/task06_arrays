package com.chekushka.model;

public class ArrayEntity {
    private int[] firstArray;
    private int[] secondArray;

    public ArrayEntity(int[] firstArray, int[] secondArray) {
        this.firstArray = firstArray;
        this.secondArray = secondArray;
    }

    public int[] getFirstArray() {
        return firstArray;
    }

    public void setFirstArray(int[] firstArray) {
        this.firstArray = firstArray;
    }

    public int[] getSecondArray() {
        return secondArray;
    }

    public void setSecondArray(int[] secondArray) {
        this.secondArray = secondArray;
    }
}
