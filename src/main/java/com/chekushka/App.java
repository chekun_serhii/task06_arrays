package com.chekushka;

import com.chekushka.view.AppView;

public class App {
    public static void main(String[] args) {
        AppView start = new AppView();
        start.show();
    }
}
