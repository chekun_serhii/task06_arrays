package com.chekushka.generics;

import com.chekushka.generics.rooms.Kitchen;
import com.chekushka.generics.rooms.Room;
import java.util.List;

public class CustomContainer {
    public class ContainerExample<T extends Room> {
        private T value;

        public T getValue() {
            return value;
        }

        public void setValue(T value) {
            this.value = value;
        }

        public boolean find(List<? extends Room> list, Room unit) {
            for (Room value : list) {
                if (unit.equals(value)) {
                    return true;
                }
            }
            return false;
        }

        public void addToList(List<? super Kitchen> list, Kitchen kitchen) {
            list.add(kitchen);
        }
    }

}
